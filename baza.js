const Sequelize = require('sequelize')
const sequelize = new Sequelize('popravni', 'root', '',{
    host: '127.0.0.1',
    dialect: 'mysql',
    define:{
        freezeTableName: true
    }
})


const db={}
db.Sequelize = Sequelize
db.sequelize = sequelize


db.predmet=sequelize.import(__dirname + '/predmet.js')
db.nastavnik=sequelize.import(__dirname + '/nastavnik.js')
db.godina=sequelize.import(__dirname + '/godina.js')
db.lekcija=sequelize.import(__dirname + '/lekcija.js')

db.predmet.hasMany(db.lekcija, {foreignKey: 'predmet'})
db.nastavnik.hasMany(db.predmet, {foreignKey: 'nastavnik'})
db.predmet.hasMany(db.predmet, {foreignKey:'preduslov'})

db.predmet.hasOne(db.godina, {foreignKey: 'predmet'})

module.exports = db