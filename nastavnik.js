const Sequelize = require('sequelize')
module.exports=function(sequelize, DataTypes){
    const nastavnik = sequelize.define('nastavnik',{
        ime : Sequelize.STRING,
        prezime : Sequelize.STRING,
        titula : Sequelize.STRING,
    })
    return nastavnik
}