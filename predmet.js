const Sequelize = require('sequelize')
module.exports=function(sequelize, DataTypes){
    const predmet = sequelize.define('predmet',{
        naziv : Sequelize.STRING,
        brojCasova : Sequelize.INTEGER
    })
    return predmet
}