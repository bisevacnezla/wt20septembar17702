const Sequelize = require('sequelize')
module.exports=function(sequelize, DataTypes){
    const lekcija = sequelize.define('lekcija',{
        naziv : Sequelize.STRING,
        brojCasova : Sequelize.INTEGER
    })
    return lekcija
}