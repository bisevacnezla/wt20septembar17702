const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const db = require('./baza.js')

const app = express()
app.use(bodyParser.json)
app.use(bodyParser.urlencoded({extended:true}))
app.use(express.static(__dirname))

db.sequelize.sync({force:true}).then(function(){
    inicijalizacija().then(function(){
        console.log('Zavrseno popunjavanje tabela')
    })
})


function inicijalizacija(){
    var predmetiListaPromisea = []
    var lekcijeListaPromisea = []
    var godineListaPromisea = []
    var nastavniciListaPromisea = []

    return new Promise(function(resolve,reject){
        nastavniciListaPromisea.push(db.nastavnik.create({ime : 'Vensada', prezime: 'Okanovic', titula:'red.prof'}))
        nastavniciListaPromisea.push(db.nastavnik.create({ime : 'Huse', prezime: 'Fatkic', titula:'red.prof'}))
        
        predmetiListaPromisea.push(db.predmet.create({naziv: 'WT', brojCasova: 40, preduslov: null, nastavnik: 1}))
        predmetiListaPromisea.push(db.predmet.create({naziv: 'IM1', brojCasova: 50, preduslov: null, nastavnik: 2}))
        predmetiListaPromisea.push(db.predmet.create({naziv: 'IM2', brojCasova: 50, preduslov: 2, nastavnik: 2}))

        lekcijeListaPromisea.push(db.lekcija.create({naziv : 'CSS', brojCasova: 2, predmet: 1}))
        lekcijeListaPromisea.push(db.lekcija.create({naziv : 'Integrali', brojCasova: 8, predmet: 2}))

        godineListaPromisea.push(db.godina.create({naziv : 'prva', predmet: 2}))
    })
}

    app.post('/lekcija', function(req,res){
        let tijelo = req.body
        let naziv=tijelo.naziv
        let predmet=tijelo.predmet
        let brojCasova = tijelo.brojCasova

        db.lekcija.findOne({where: {naziv: naziv}})
    })

    app.get('/nastavnik/:id/brojcasova', function(req,res){
        
    })
    app.listen(8085, ()=>{
        console.log('Server je na portu 8085')
    })
